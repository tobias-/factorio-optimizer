import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.3.11"
}

group = "be.olsson"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    compile(kotlin("stdlib-jdk8"))
    compile("com.fasterxml.jackson.core:jackson-databind:2.9.5")
    compile("org.dizitart:potassium-nitrite:3.1.0")
    compile("org.slf4j:slf4j-simple:1.7.25")
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}


task("run", JavaExec::class) {
    classpath = sourceSets["main"].runtimeClasspath
    main = "be.olsson.StartKt"
    dependsOn("assemble")
}
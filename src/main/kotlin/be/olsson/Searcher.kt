package be.olsson

import be.olsson.database.Product
import be.olsson.database.Recipe
import be.olsson.database.Repo
import org.dizitart.kno2.filters.and
import org.dizitart.kno2.filters.elemMatch
import org.dizitart.kno2.filters.eq
import org.dizitart.kno2.filters.gt
import kotlin.math.ceil

object Searcher {


    fun getRecipiesForProduct(product: String) = Repo
        .recipies
        .find(
            Recipe::products elemMatch (
                (Product::name eq product)
                    and
                    (Product::amount gt 0.0)
                )
        )
        .filterNotNull()

    private fun getRecipe(recipe: String) = Repo
        .recipies
        .find(
            Recipe::name eq recipe
        )
        .first()!!

    private fun addRequiredRecipiesForProduct(
        productName: String,
        amount: Double,
        cache: RecipeCache,
        shoppingList: MutableMap<String, Double>,
        surplus: MutableMap<String, Double>
    ) {

        val requiredAmount = surplus.withdrawWithRest(productName, amount)
        if (requiredAmount < 0.0000001) {
            return
        }

        val process = cache[productName]

        val productOutput = process.products.first { it.name == productName }

        val processesCount = ceil(requiredAmount / productOutput.amount)
        shoppingList.add(process.name, processesCount)
        for ((surplusProductName, surplusAmount) in process.products) {
            if (surplusProductName == productName) {
                surplus.add(surplusProductName, surplusAmount * processesCount - requiredAmount)
            } else {
                surplus.add(surplusProductName, surplusAmount * processesCount)
            }
        }

        if (process.category == "barreling-pump") {
            // Full barrels are assumed to be available
            return
        }
        for ((ingredientName, ingredientAmount) in process.ingredients) {
            addRequiredRecipiesForProduct(
                ingredientName,
                processesCount * ingredientAmount,
                cache,
                shoppingList,
                surplus
            )
        }
    }

    fun getRequiredRecipiesForProduct(product: String, amount: Double, preferredRecipies: Set<String>): RecipeBuilder {
        val shoppingList = linkedMapOf<String, Double>()
        val surplus = linkedMapOf<String, Double>()
        addRequiredRecipiesForProduct(product, amount, RecipeCache(preferredRecipies), shoppingList, surplus)
        return RecipeBuilder(
            shoppingList
                .map { (recipe, amount) ->
                    RecipeRequirement(
                        amount,
                        getRecipe(recipe)
                    )
                },
            surplus.filterValues { it > 0.0 }
        )
    }

    private fun MutableMap<String, Double>.add(key: String, term: Double): Double {
        return compute(key) { _, value ->
            (value ?: 0.0) + term
        }!!
    }

    private fun MutableMap<String, Double>.withdrawWithRest(key: String, required: Double): Double {
        val orig = this[key] ?: 0.0
        val diff = required - orig
        if (diff > 0) {
            this[key] = 0.0
            return diff
        } else {
            this[key] = -diff
            return 0.0
        }
    }
}

class RecipeCache(
    private val preferredRecipies: Set<String>
) {
    private val cache = mutableMapOf<String, Recipe>()

    operator fun get(productName: String): Recipe {
        return cache.computeIfAbsent(productName) {

            val viableProcesses = Searcher.getRecipiesForProduct(productName).associateBy { it.name }
            val processes = if (viableProcesses.size == 1) {
                viableProcesses.keys
            } else {
                viableProcesses.keys intersect preferredRecipies
            }
            if (processes.isEmpty()) {
                throw MultipleRecipiesException("No preferred process matches $productName", viableProcesses.values)
            }
            viableProcesses[processes.first()]!!
        }
    }


}

data class RecipeBuilder(
    val recipeRequirements: List<RecipeRequirement>,
    val surplus: Map<String, Double>
) {
    override fun toString(): String {
        val reqs = recipeRequirements.joinToString("\n") { "    $it" }
        val surplus = surplus.entries.joinToString("\n") { (key, amount) -> "    $key: $amount" }
        return "surplus: [\n$surplus\n]\nrecipe: [\n$reqs\n]"
    }
}

data class RecipeRequirement(
    val amount: Double,
    val recipe: Recipe
) {
    val time: Double
        get() = recipe.time * amount

    override fun toString() = "${recipe.name}: $amount (time: $time)"
}

class MultipleRecipiesException(
    message: String,
    availableRecipies: Collection<Recipe>
) : RuntimeException(message + "\n" + availableRecipies.joinToString("\n"))
package be.olsson

fun main(args: Array<String>) {
    val product = "advanced-processing-unit"
    val amountPerMinute = 1000.0

    val preferredRecipies = linkedSetOf(
        "angels-roll-iron-converting",
        "roll-iron-casting-fast",
        "molten-iron-smelting-1",
        "pellet-iron-smelting",
        "solid-limestone",
        "washing-3",
        "washing-2",
        "washing-1",
        "empty-water-viscous-mud-barrel",
        "sb-wood-bricks-charcoal",
        "cellulose-fiber-raw-wood",
        "tree-arboretum-3",
        "tree-generator-3",
        "angels-bio-void-swamp-1",
        "swamp-1-seed",
        "gas-urea",
        "gas-ammonia",
        "angelsore-crushed-mix1-processing",
        "angelsore-crushed-mix2-processing",
        "angelsore-crushed-mix3-processing",
        "angelsore-crushed-mix4-processing",
        "angelsore-crushed-mix5-processing",
        "angelsore-crushed-mix6-processing",
        "slag-processing-1",
        "slag-processing-2",
        "slag-processing-3",
        "slag-processing-4",
        "slag-processing-5",
        "slag-processing-6",
        "angelsore1-crushed",
        "angelsore2-crushed",
        "angelsore3-crushed",
        "angelsore4-crushed",
        "angelsore5-crushed",
        "angelsore6-crushed",
        "slag-processing-filtering-2",
        "filter-ceramic-refurbish",
        "slag-processing-dissolution",
        "dirt-water-separation",
        "empty-liquid-sulfuric-acid-barrel",
        "air-separation",
        "angels-air-filtering",
        "carbon-separation-2",
        // "coolant-used-filtration-1"
        "empty-liquid-coolant-barrel",
        "angels-roll-solder-converting",
        "roll-solder-casting-fast",
        "angels-solder-smelting-1",
        "solid-lead-oxide-smelting",
        "coke-purification",
        "processed-lead-smelting",
        "pellet-tin-smelting",
        "angels-wire-coil-tin-converting",
        "angels-wire-coil-tin-casting-fast",
        "anode-copper-smelting",
        "wooden-board",
        "wood",
        "angels-wire-coil-copper-converting",
        "angels-wire-coil-copper-casting-fast",
        "solid-plastic",
        "liquid-plastic-1",
        "gas-propene-synthesis",
        "gas-methanol-from-wood",
        "coolant-cool-300",
        "angels-plate-silicon",
        "silicon-ore-smelting",
        "angelsore1-crystal-processing"
    )

    val x = Searcher
        .getRequiredRecipiesForProduct(
            product,
            amountPerMinute,
            preferredRecipies
        )

    System.err.println(x)
}
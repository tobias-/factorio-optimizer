package be.olsson.database

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ObjectNode
import org.dizitart.kno2.getRepository
import org.dizitart.kno2.nitrite
import org.dizitart.no2.IndexType
import org.dizitart.no2.objects.Index
import org.dizitart.no2.objects.Indices
import org.dizitart.no2.objects.ObjectRepository
import java.nio.charset.StandardCharsets

object Repo {
    val db by lazy {
        nitrite {
            autoCommitBufferSize = 2048
            compress = true
            autoCommit = false
        }
    }

    val recipies = db.getRepository<Recipe> {
        bootstrapDatabase()
    }

    private fun ObjectRepository<Recipe>.bootstrapDatabase() {
        val str = javaClass.getResourceAsStream("/recipies.json").readBytes()
            .toString(StandardCharsets.UTF_8)
        val tree = ObjectMapper()
            .readTree(str) as ObjectNode

        for ((key, value) in tree.fields()) {
            value as ObjectNode
            try {
                insert(
                    Recipe(
                        name = key,
                        category = value.category,
                        time = value.time,
                        products = value["products"]
                            .map { it ->
                                Product(
                                    it.name,
                                    it.amount
                                )
                            },
                        ingredients = value["ingredients"]
                            .map { it ->
                                Ingredient(
                                    it.name,
                                    it.amount
                                )
                            }
                    )
                )
            } catch (e: Exception) {
                System.err.println("$key : $value")
                e.printStackTrace()
                throw e
            }
        }
    }

    private val JsonNode.amount: Double
        get() = get("amount")
            ?.asDouble()
            ?: run {
                (get("amount_min").asDouble() + get("amount_max").asDouble()) / 2
            }

    private val JsonNode.name: String
        get() = get("name").asText()

    private val JsonNode.category: String
        get() = get("category").asText()

    private val JsonNode.time: Int
        get() = get("energy").asInt()
}

@Indices(
    Index(value = "name", type = IndexType.Unique)
    //Index(value = "ingredients.name", type = IndexType.NonUnique)
)
data class Recipe(
    val name: String,
    val category: String,
    val time: Int,
    val products: List<Product>,
    val ingredients: List<Ingredient>
)

data class Ingredient(
    val name: String,
    val amount: Double
)

data class Product(
    val name: String,
    val amount: Double
)